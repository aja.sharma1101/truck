const request = require('supertest');
const mongoose = require("mongoose");
const messages = require('../models/messages');
const messageData = require('./messages').messages;
// Load any environment variables from .env files in the parent directory.
require('dotenv').config();

let messageValues = {};
messageData.map((value)=>{
    messageValues[value.message_id] = value.message_value;
});

var app;

beforeAll(async function(){
    mongoose.connect(process.env.MONGO_URL_TEST);
    try{
        await messages.insertMessages(messageData);
        app = require("../server");
    }catch(err){
        console.log("Failed to initialise test suite");
    }
});

describe('Testing API Endpoints', () => {
    let truckId = null;
    let totalParcelWeight = 0;
    let loadedParcels = [];
    it('creates a truck', async () => {
        const res = await request(app.server)
            .post('/addTruck')
            .send({
                truck_name: 'Truck 1',
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('truck_id');
        expect(res.body.message).toBe(messageValues["INSERT_TRUCK_SUCCESS"]);
        truckId = res.body.truck_id;
    });

    it('creates 10 parcels for the truck of incremental weights from 1-10', async () => {
        for(let i=1; i<=10; i++){
            const res = await request(app.server)
            .post('/loadParcel')
            .send({
                truck_id: truckId,
                parcel_weight:i
            });
            expect(res.statusCode).toEqual(200);
            expect(res.body).toHaveProperty('parcel_id');
            expect(res.body.message).toBe(messageValues.LOAD_PARCEL_SUCCESS);
            loadedParcels.push(res.body.parcel_id);
            totalParcelWeight = totalParcelWeight + i
        }
    });

    it('deletes two parcels of weights 8 and 9', async () => {
        for(let i=7;i<9;i++){
            const res = await request(app.server)
            .delete('/unloadParcel/'+loadedParcels[i])
            .send();
            expect(res.statusCode).toEqual(200);
            expect(res.body.message).toBe(messageValues.UNLOAD_PARCEL_SUCCESS);
            totalParcelWeight = totalParcelWeight - (i+1)
        }
    });

    it('fetches the truck details and total weight', async () => {
        for(let i=1; i<=10; i++){
            const res = await request(app.server)
            .get('/getTrucks')
            .send();
            expect(res.statusCode).toEqual(200);
            expect(Array.isArray(res.body)).toBe(true);
            expect(res.body[0]).toHaveProperty('total_weight');
            expect(res.body[0]).toHaveProperty('parcel_count');
            expect(res.body[0].parcel_count).toBe(8);
            expect(res.body[0].total_weight).toBe(totalParcelWeight);
        }
    });
});

afterAll(async function () {
    await mongoose.connection.db.dropDatabase();
    await app.db_client.disconnect();
    await app.instance.close();
})