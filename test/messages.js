module.exports.messages = [
    {
        message_id:"TRUCK_NAME_NOT_FOUND",
        message_value:"Truck name not found"
    },
    {
        message_id:"INSERT_TRUCK_SUCCESS",
        message_value:"Truck successfully added."
    },
    {
        message_id:"INSERT_TRUCK_FAILED",
        message_value:"Truck successfully failed."
    },
    {
        message_id:"PARCEL_WEIGHT_NOT_FOUND",
        message_value:"Parcel weight not found."
    },
    {
        message_id:"TRUCK_ID_NOT_FOUND",
        message_value:"Truck ID not found."
    },
    {
        message_id:"LOAD_PARCEL_SUCCESS",
        message_value:"Parcel successfully loaded to truck."
    },
    {
        message_id:"LOAD_PARCEL_FAILED",
        message_value:"Loading parcel failed."
    },
    {
        message_id:"UNLOAD_PARCEL_SUCCESS",
        message_value:"Parcel successfully unloaded from truck."
    },
    {
        message_id:"UNLOAD_PARCEL_FAILED",
        message_value:"Unloading parcel failed."
    },
    {
        message_id:"FETCH_TRUCKS_FAILED",
        message_value:"Error fetching truck details."
    }
]