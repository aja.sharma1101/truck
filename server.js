'use strict';

// import required libraries
// express library for API specifications
const express = require("express");
// Mongoose library for connecting to mongodb for data persistence.
const mongoose = require("mongoose");
const cors=require('cors');
// Initialise the web API
const app = express();

// Bodyparser library to parse JSON request body in the express library.
const bodyParser = require('body-parser');
// Load any environment variables from .env files in the parent directory.
require('dotenv').config();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// declare constants
const PORT = 8081;

if(app.get('env')=="test"){
    mongoose.connect(process.env.MONGO_URL_TEST);
    module.exports.db_client = mongoose;
}else if(app.get('env')=="development"){
    app.use(cors({origin: [ process.env.ANGULAR_URL
      ], credentials: true}));
    mongoose.connect(process.env.MONGO_URL)
}else{
    mongoose.connect(process.env.MONGO_URL);
}

const messages = require("./models/messages");
const truck = require("./models/truck");
const parcels = require("./models/parcels");

let messageData = {};
  
// API to add a Truck
app.post("/addTruck",async (req,res)=>{
    // Initialise user credits
    let truckData = req.body;
    if(!truckData || !truckData.truck_name){
        return handleError(res,"TRUCK_NAME_NOT_FOUND",403);
    }
    try{
        let result = await truck.insertTruck(truckData);
        handleSuccess(res, "INSERT_TRUCK_SUCCESS", {truck_id: result._doc._id});
    }catch(err){
        console.log(err);
        handleError(res, "INSERT_TRUCK_FAILED");
    }
})

// API to load a parcel onto a truck
app.post("/loadParcel",async (req,res)=>{
    // Initialise user credits
    let parcelData = req.body;
    if(!parcelData || !parcelData.parcel_weight){
        return handleError(res,"PARCEL_WEIGHT_NOT_FOUND");
    }else if(!parcelData.truck_id){
        return handleError(res,"TRUCK_ID_NOT_FOUND");
    }
    try{
        let result = await parcels.insertParcel(parcelData);
        handleSuccess(res, "LOAD_PARCEL_SUCCESS", {parcel_id: result._doc._id});
    }catch(err){
        handleError(res, "LOAD_PARCEL_FAILED");
    }
});

// API to unload a parcel
app.delete("/unloadParcel/:id",async (req,res)=>{
    let truckId = req.params.id;
    try{
        await parcels.deleteParcel(truckId);
        handleSuccess(res, "UNLOAD_PARCEL_SUCCESS");
    }catch(err){
        handleError(res, "UNLOAD_PARCEL_FAILED");
    }
});

// API to fetch trucks and their details
app.get("/getTrucks",async (req,res)=>{
    try{
        let result = await truck.getTruckDetails();
        res.json(result);
    }catch(err){
        console.log(err);
        handleError(res, "FETCH_TRUCKS_FAILED");
    }
});

function handleError(res, message_id, statusCode=403){
    res.statusCode = statusCode;
    res.send({
        error:messageData[message_id]
    })
}

function handleSuccess(res, message_id, body={}){
    let result = {
        message:messageData[message_id],
        ...body
    }
    res.json(result);
}

async function initialise(){
    // Fetch the information for server messages.
    messageData = await messages.getMessages();
    module.exports.instance = app.listen(PORT);
} 

initialise();

module.exports.server = app;