const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const messagesSchema = new Schema({
    message_id: String,
    message_value: String,
 });
 const messages = mongoose.model('Messages', messagesSchema);

module.exports.getMessages = function(){
    return new Promise((resolve, reject)=>{
        var query = messages.find({});
        query.select("-__v");
        query.lean().exec(function (error, result) {
            if (error) {
                console.log(error);
                return reject(error);
            }
            messageData = {}
            result.forEach((msgInfo)=>{
                messageData[msgInfo.message_id] = msgInfo.message_value;
            });
            resolve(messageData);
        });
    });
}

module.exports.insertMessages = function(data){
    return messages.insertMany(data);
}

module.exports.messages = messages;
