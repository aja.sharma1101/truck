const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const trucksSchema = new Schema({
    truck_name: {
        type: String,
        required: true
    }
});
const truck = mongoose.model('Truck', trucksSchema);

module.exports.getTruckDetails = function () {
    return new Promise((resolve, reject) => {
        let query = truck.aggregate([{
            $lookup: {
                from: "parcels", // collection name in db
                localField: "_id",
                foreignField: "truck_id",
                as: "parcels"
            }
        }, {
            $unwind: { path: "$parcels", preserveNullAndEmptyArrays: true }
        }, 
        {
            $group:
            {
                _id: "$_id",
                total_weight: {
                    $sum: "$parcels.parcel_weight"
                },
                parcel_count: { $sum: 1 }
            }
        }]);

        query.exec(function (error, result) {
            if (error) {
                reject(error);
            }
            resolve(result);
        });
    })
}

module.exports.insertTruck = function (truckDetails) {
    return new Promise((resolve, reject) => {
        let newTruck = new truck(truckDetails);
        newTruck.save(function (err, result) {
            if (err) return reject(err);
            resolve(result);
        });
    });
}

module.exports.truck = truck;
