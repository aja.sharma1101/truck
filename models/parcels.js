const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const parcelsSchema = new Schema({
    truck_id: {
        type:Schema.Types.ObjectId,
        required: true
    },
    parcel_weight: {
        type:Number,
        required: true
    }
 });
 const parcel = mongoose.model('Parcel', parcelsSchema);


module.exports.insertParcel = function(parcelData){
    let newParcel = new parcel(parcelData);
    return newParcel.save();
}

module.exports.deleteParcel = function(parcelId){
    return parcel.remove({_id:parcelId});
}

module.exports.parcel = parcel;
